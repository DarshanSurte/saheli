var app = require('express').Router();
var login = require('./login');
app.post('signup', login.signup);
app.get('users', login.getAll);
app.post('login', login.login);
module.exports = app;