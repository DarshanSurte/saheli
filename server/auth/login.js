var utils = require('../utils/utils');

var loginobj = {
    login : function(req,res) {
        let user = req.body;

        res.send({"msg":"Login success"})
    },
    signup : function(req,res){
        let user = req.body;
        utils.addObject('users',user).then(success => {
            res.send(success);
        }).catch(error => {
            res.send(error);
        })
    },
    getAll: function(req,res){
        utils.getAllObjects('users').then(success => {
            res.send(success);
        }).catch(error => {
            res.send(error);
        })
    }
};

module.exports = loginobj;