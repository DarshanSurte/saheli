import { Component } from '@angular/core';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  allinsights:any = [
    { title: 't1', subtitle: 'st1', text: 'adsdfsvbvmnmndf'},
    { title: 't2', subtitle: 'st2', text: 'adsdfsdsddmnf'},
    { title: 't3', subtitle: 'st3', text: 'adsdfsdkjkjkkjf'},
    { title: 't4', subtitle: 'st4', text: 'adsdfstrrttrtdf'},
    { title: 't5', subtitle: 'st5', text: 'adsdfsdsdfsdff'},
  ];
  constructor() {}

}
