import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './users/users.component';
import { UserdetailsComponent } from './userdetails/userdetails.component';
import {RouterModule,Routes} from '@angular/router';
import { ArticlesComponent } from './articles/articles.component';
import { UserdetailComponent } from './userdetail/userdetail.component';
import { ChatComponent } from './chat/chat.component';
var routes:Routes = [
  {
    path: 'users', component: UsersComponent
  }, { path: 'userdetails', component: UserdetailsComponent},
  { path: '', redirectTo: 'users', pathMatch: 'full'}, 
  { path: 'articles', component: ArticlesComponent},
  {path: 'chat', component: ChatComponent}

];

@NgModule({
  declarations: [UsersComponent, UserdetailsComponent, ArticlesComponent, UserdetailComponent, ChatComponent],
  imports: [
    CommonModule, RouterModule.forChild(routes)
  ]
})
export class AdminModule { }
