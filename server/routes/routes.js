var app = require('express').Router();
var auth = require('../auth/login-routes');
app.use('/', auth);

module.exports = app;